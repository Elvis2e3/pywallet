from django.contrib import admin

# Register your models here.
from wallet.models import Coin, ExchangeCurrency, Key

admin.site.register(ExchangeCurrency)
admin.site.register(Coin)
admin.site.register(Key)
