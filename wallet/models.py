from django.db import models


class ExchangeCurrency(models.Model):
    name = models.CharField(max_length=50)
    value = models.FloatField()

    def __str__(self):
        return self.name


class Coin(models.Model):
    symbol = models.CharField(max_length=50)
    amount = models.FloatField(default=0.0)

    def __str__(self):
        return self.symbol



class Key(models.Model):
    api_key = models.CharField(max_length=100)
    api_secret = models.CharField(max_length=100)
