from django.views.generic import TemplateView

from binance import Client

from wallet.models import Key, Coin, ExchangeCurrency


class PyWallet(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        key_api = Key.objects.last()
        client = Client(
            key_api.api_key,
            key_api.api_secret
        )
        coins_data = []
        coins = Coin.objects.all()
        exchange_currency = ExchangeCurrency.objects.last()
        total_sus = 0.0
        total_bob = 0.0
        for coin in coins:
            data = {}
            get_data_coin = client.get_orderbook_ticker(symbol=coin.symbol)
            data["coin"] = coin.symbol
            data["price"] = get_data_coin["askPrice"]
            data["amount"] = coin.amount
            data["price_sus"] = float(get_data_coin["askPrice"]) * coin.amount
            data["price_bob"] = data["price_sus"] * exchange_currency.value
            coins_data.append(data)
            total_sus += data["price_sus"]
            total_bob += data["price_bob"]
        coins_data.append({
            "coin": "TOTAL",
            "price": "",
            "amount": "",
            "price_sus": total_sus,
            "price_bob": total_bob,
        })
        context["coins"] = coins_data
        return context
